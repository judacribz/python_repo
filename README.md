# Python Repo README #

## (CSCI 3010U) Simulation and Modelling:
Labs                          | Assignments
------------------------------|-----------------------------------
[Lab 1 - 1D Ball in Freefall](csci_3010u/lab/Lab1/) | [A1 - Falling Slinky](csci_3010u/ass/A1/)  
[Lab 2 - Projectile Motion](csci_3010u/lab/Lab2/) | [Assignment 2](csci_3010u/ass/A2/)  
[Lab 3 - Earth and Moon Motion](csci_3010u/lab/Lab3/) |
[Lab 4](csci_3010u/lab/Lab4/) |
[Lab 5](csci_3010u/lab/Lab5/) |
[Lab 6](csci_3010u/lab/Lab6/) |
[Lab 7](csci_3010u/lab/Lab7/) |
[Lab 8](csci_3010u/lab/Lab8/) |
[Lab 9](csci_3010u/lab/Lab9/) |
[Lab 10](csci_3010u/lab/Lab10/)  |  




